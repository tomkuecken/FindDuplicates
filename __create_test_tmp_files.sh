#!/bin/bash
#
# Unique Files:
# 	a_uniq1
# 	a_uniq2
# Duplicates: different name
# 	a_dup1_a
# 	a_dup1_b
# Duplicates: same name	
# 	a_dup2
# 	a_dup2
# ===============================

DIR="/tmp/a_dup_test"

if [ -d "$DIR" ]; then
	rm -r "$DIR"
fi

mkdir /tmp/a_dup_test/
cd /tmp/a_dup_test/

# unique
echo "unique1" > a_uniq1;
echo "unique2" > a_uniq2;

# dup - different name
echo "dup1" > a_dup1_a;
echo "dup1" > a_dup1_b;

# dup - same name, different dirs 
echo "dup2" > a_dup2 
mkdir a
cd a
echo "dup2" > a_dup2

# dup - same name && diff name && diff dirs 
cd ..;
echo "dup3" > a_dup_3;
echo "dup3" > a_dup_3;
echo "dup3" > a_dup_3_a;
echo "dup3" > a/a_dup_3 ;
echo "dup3" > a/a_dup_3;
echo "dup3" > a/a_dup_3_a;
echo "dup3" > a/a_dup_3_b;
