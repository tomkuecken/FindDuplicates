import os
import hashlib
from collections import defaultdict
from datetime import datetime as dt
import _mssql

import logging
from logging.config import fileConfig

import sys
sys.path.append('..')

from BaseUtils import StringUtil

DB = {
    'host': '192.168.100.159',
    'user': 'sa',
    'password': 'PASSWORD',
    'database': 'FilesystemInfo',
    'table': 'FileInfo'
}

LOGGING_LEVEL = logging.INFO

def get_logger():
    logging.basicConfig(level=LOGGING_LEVEL)
    return logging.getLogger(__name__)


class FileUtils(object):
    @staticmethod
    def get_home_dir():
        return os.path.expanduser('~')

    # TODO: Want to use different lib (e.g. xxhash)?
    @staticmethod
    def calc_hash(file_path, hash_type=hashlib.md5(), block_size=2 ** 20 * 60):
        m = hash_type
        with open(file_path, 'rb') as f:
            chunk = 0
            while chunk != b'':
                chunk = f.read(block_size)
                m.update(chunk)

        return m.hexdigest()

    @staticmethod
    def enumerate_files(paths: ()):
        file_collection = []
        total = len(paths)
        start = dt.now()

        print('Starting to enumerate paths: {0}'.format(str(start)))
        for i, path in enumerate(paths):
            print('Enumerating paths - [ {0} / {1} ]'.format(str(i + 1), str(total)))
            print(path)
            print('Total files: {0}'.format(str(len(file_collection))))

            for rootdir, subdirs, files in os.walk(path):
                for file in files:
                    fullpath = os.path.abspath(os.path.join(rootdir, file))
                    if not os.path.isfile(fullpath):
                        continue

                    try:
                        file_collection.append(fullpath)
                    except:
                        print('Failed to enum %s' % path)

        print('Done. Total time: {0}\n\n'.format(str(dt.now() - start)))
        return file_collection

    @staticmethod
    def calc_crc32(file_path: str):
        if not file_path:
            return

        import binascii

        with open(file_path, 'rb') as f:
            buf = f.read()
            buf = (binascii.crc32(buf) & 0xFFFFFFFF)
            return "%08X" % buf


class DuplicateCollection(object):
    def __init__(self, file_paths: [], db: {}, table_name: str, logger: logging.Logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = get_logger()

        self.paths = file_paths
        self.files = FileUtils.enumerate_files(self.paths)
        self.conn = _mssql.connect(db['host'], db['user'], db['password'], db['database'])
        self.tablename = table_name
        self.failed = []

        self.fileinfos = []
        # self.get_file_infos()

    def create_table(self):
        """Description:
            Creates table to store file infos
            Will create a temp table to insert vals in initially
                This will let us not be able to resume work and not have to enumerate or re-hash files

            The table is not normalized
            Creates indexes on the File_Path and access times
        """
        cur = self.conn.cursor()
        try:
            cur.execute("""USE [FilesystemInfo]
                            GO

                            SET ANSI_NULLS ON
                            GO

                            SET QUOTED_IDENTIFIER ON
                            GO

                            IF NOT EXISTS (
                                SELECT * FROM sys.tables WHERE name = 'FileInfo'
                            )
                            BEGIN
                                CREATE TABLE [dbo].[FileInfo](
                                    [FileInfo_ID] [int] IDENTITY(1,1) NOT NULL,
                                    [Batch_ID] [int] NOT NULL,
                                    [Full_Path] [varchar](max) NOT NULL,
                                    [Size] [int] NULL,
                                    [Md5] [nvarchar](1000) NULL,
                                    [Sha256] [nvarchar](1000) NULL,
                                    [A_Time] [float] NULL,
                                    [C_Time] [float] NULL,
                                    [M_Time] [float] NULL,
                                    [Date_Added_UTC] [datetime] NULL,
                                    [Last_Updated_UTC] [datetime] NULL,
                                    [Processed_Flag] [bit] NOT NULL,
                                    [Active_Flag] [bit] NOT NULL
                                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                            END
                            GO

                            IF NOT EXISTS (
                                SELECT *
                                FROM sys.indexes i
                                JOIN sys.objects o ON i.object_id = o.object_id
                                WHERE
                                    o.name = 'FileInfo'
                                    AND (
                                        i.type_desc = 'CLUSTERED'
                                        AND i.name = 'IX_FileInfo_Full_Path'
                                    )
                            )
                            BEGIN
                                CREATE CLUSTERED INDEX [IX_FileInfo_Full_Path] ON [dbo].[FileInfo]
                                (
                                    [FileInfo_ID] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                            END
                            GO

                            IF NOT EXISTS (
                                SELECT *
                                FROM sys.indexes i
                                JOIN sys.objects o ON i.object_id = o.object_id
                                WHERE
                                    o.name = 'FileInfo'
                                    AND (
                                        i.type_desc = 'CLUSTERED'
                                        AND i.name = 'IX_FileInfo_Full_Path'
                                    )
                            )
                            BEGIN
                                CREATE NONCLUSTERED INDEX [IX_FileInfo_Size] ON [dbo].[FileInfo]
                                (
                                    [FileInfo_ID] ASC
                                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                            END
                            GO""")
        except _mssql.MSSQLException as err:
            self.logger.error('Fauled to create initial table with indexes')
            self.logger.error(err)
        finally:
            cur.close()

    def existing_md5(self):
        """
        Description:
            Gets list of existing paths in the database that:
                - Have an MD5 hash
                - Inactive_Flag = 0
                - Processed_Flag = 0
        :return:
        """
        cur = self.conn.cursor()
        existing = []
        try:
            cur.execute("""SELECT Full_Path
                            FROM FileInfo
                            WHERE
                                ISNULL(Processed_Flag, 0) = 0
                                AND ISNULL(Inactive_Flag, 0) = 0""")
            existing = cur.fetchall()
        except _mssql.MSSQLException as e:
            self.logger.error('Failed to get existing md5 paths')
            self.logger.error(e)
        finally:
            cur.close()

        return existing

    def get_new_paths_to_check(self):
        """
        Description:
            Compares list of current paths with those already in the database
            Returns paths currently without an MD5, so they can be calculated
        :return:
        """
        paths = []
        existing = self.existing_md5()

        if not existing:
            return self.files

        for file in self.files:
            if file in existing:
                continue
            paths.append(file)

        return paths

    def clean_old_files(self):
        """
        Description:
            1. Gets list of paths in the database that are:
                - Not inactive
                - Not processed
            2. Checks if the paths exist
                > They'll be marked inactive if they do not exist
        :return:
        """
        cur = self.conn.cursor()
        dbfiles = []
        try:
            cur.execute("""SELECT File_Path
                           FROM FileInfo
                           WHERE IFNULL(Inactive_Flag, 0) <> 1
                               AND IFNULL(Processed_Flag, 0) <> 1""")
            dbfiles = cur.fetchall()
        except _mssql.MSSQLException as e:
            print(e)
        finally:
            cur.close()

        if not dbfiles:
            return

        inactive_paths = []
        for file in dbfiles:
            if not os.path.exists(file):
                inactive_paths.append(file)

        if not inactive_paths:
            return

        cur = self.conn.cursor()
        try:
            for inactive in inactive_paths:
                try:
                    cur.execute("""UPDATE FileInfo
                                    SET Inactive_Flag = 1
                                    WHERE File_Path = ?""", inactive)
                except _mssql.MSSQLException as e:
                    print('Failed to mark path inactive:')
                    print(inactive)
                    print(e)
            self.conn.commit()
        except _mssql.MSSQLException as err:
            self.conn.rollback()
            print(err)
        except:
            self.conn.rollback()
            print('Failed to update any rows')
        finally:
            cur.close()


    def get_file_info(self, files):
        """
        Description:
            Gets initial info about files:
                - Size
                - Access/Modified times
                - MD5 hash
            Then inserts the rows in the database
        :param files:
        :return:
        """
        infos = []
        total = len(self.files)
        start = dt.now()

        for i, path in enumerate(self.files):
            print('Getting fizes and md5 ({0}): {1} / {2}: {3}'.format(str(start), str(i + 1), str(total), path))
            try:
                row = (
                    path,
                    os.path.getsize(path),
                    os.path.getatime(path),
                    os.path.getctime(path),
                    os.path.getmtime(path),
                    str(FileUtils.calc_hash(path, hashlib.md5())),
                    # str(hash(path)),
                    str(dt.utcnow())
                )
                infos.append(row)
            except:
                print('Failed to process %s' % path)
        print('Done getting infos. Total Time: {0}'.format(str(dt.now() - start)))

        if not infos:
            return

        cur = self.conn.cursor()
        try:
            cur.executemany("""INSERT INTO FileInfo (
                                    File_Path,
                                    Size,
                                    A_Time,
                                    C_Time,
                                    M_Time,
                                    Md5,
                                    Last_Updated_UTC
                                )
                                VALUES ( ?, ?, ?, ?, ?, ?, ? )""", infos)
            self.conn.commit()
        except _mssql.MSSQLException as e:
            self.conn.rollback()
            print(e)
        finally:
            cur.close()


    def update_db_sizes(self):
        cur = self.conn.cursor()
        existing = []
        try:
            cur.execute(""" SELECT Full_Path
                            FROM FileInfo
                            WHERE
                                ISNULL(Size, '') <> ''
                                AND ISNULL(Processed_Flag, 0) = 0
                                AND ISNULL(Inactive_Flag, 0) = 0""")
            existing = cur.fetchall()
        except _mssql.MSSQLException as e:
            self.logger.error('Failed to get existing sizes')
            self.logger.error(e)
        finally:
            cur.close()

        paths = []
        if not existing:
            paths = self.paths
        else:
            for path in self.paths:
                if path not in existing:
                    paths.append(path)
        infos = []
        total = len(paths)
        start = dt.now()

        for i, path in enumerate(paths):
            print('Getting fizes and md5 ({0}): {1} / {2}: {3}'.format(str(start), str(i + 1), str(total), path))
            try:
                row = (
                    path,
                    os.path.getsize(path)
                )

                infos.append(row)
            except:
                self.logger.warning('Failed update Md5 for path %s' %sys)


        if not infos:
            self.info('No paths found for duplicate sizes')
            return

        total = len(infos)
        start = dt.now()

        print('Updating Sizes...')
        with self.conn.cursor() as cur:
            for i, row in enumerate(infos):
                print('Inserting/Updating file sizes ({0}): {1} / {2}'.format(str(start), str(i + 1), str(total)))
                try:
                    cur.execute("""
                                IF NOT EXISTS (
                                    SELECT *
                                    FROM dbo.FileInfo
                                    WHERE ( ISNULL(Processed_Flag, 0) = 0 AND ISNULL(Inactive_Flag, 0) = 0 )
                                        AND Full_Path = '{0}'
                                )
                                BEGIN
                                    UPDATE FileInfo
                                    SET Size = '{1}'
                                        ,Last_Updated_UTC = GETUTCDATE()
                                        ,Inactive_Flag = 0
                                        ,Processed_Flag = 0
                                    WHERE Full_Path = '{0}'
                                END
                                ELSE
                                BEGIN
                                    INSERT INTO FileInfo ( Full_Path, Size, Last_Updated_UTC, Processed_Flag, Inactive_Flag )
                                    VALUES ('{0}', '{1}', GETUTCDATE(), 0, 0)
                                END""".format(row[0], row[1]))
                except _mssql.MSSQLException as ex:
                    self.logger.error('Failed to update/insert row: %s' % row)
                    self.logger.error(ex)
        print('Done. Time: {0}'.format(str(dt.now() - start)))

    def get_duplicate_rows(self, row_name: str):
        if not row_name:
            return

        dups = []
        with self.conn.cursor() as cur:
            try:
                cur.execute(""" SELECT Full_Path
                                FROM dbo.FileInfo
                                WHERE ( ISNULL(Processed_Flag, 0) = 0 AND ISNULL(Inactive_Flag, 0) = 0 )
                                GROUP BY {0}
                                HAVING COUNT({0}) > 1""".format(row_name))
                for result in cur.fetchall():
                    dups.append(result)
            except _mssql.MSSQLException as e:
                self.logger.error('Failed to duplicate file sizes')
                self.logger.error(e)

        return dups

    def get_duplicate_sizes(self):
        return self.get_duplicate_rows('Size')

    def get_duplicate_md5(self):
        return self.get_duplicate_rows('Md5')

    def get_duplicate_sha256(self):
        return self.get_duplicate_rows('Sha256')

    def get_md5(self):
        dup_sizes = self.get_duplicate_sizes()
        md5info = []

        for path in dup_sizes:
            try:
                md5info.append({
                    'path': path,
                    'atime': os.path.getatime(path),
                    'ctime': os.path.getctime(path),
                    'mtime': os.path.getmtime(path)
                })
            except:
                self.logger.warning('Failed to calc MD5 hash for path %s' %sys)


if __name__ == '__main__':
    PATHS = ('/media/tom/6e482076-27df-4f21-816c-271fd484120a',
             '/media/tom/HGST1',
             '/media/tom/HGST2',
             '/media/tom/HGST3',
             '/media/tom/HGST4',
             '/media/tom/Seagate Expansion Drive',
             '/media/veracrypt1/')

    # PATHS = ('/home/tom/Dropbox', '/home/tom/Downloads')
    dc = DuplicateCollection(PATHS)
    dc.create_table()
    dc.get_file_info(dc.get_new_paths_to_check())
